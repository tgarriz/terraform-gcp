resource "google_compute_instance" "terraform" {
  project      = "oceanic-oxide-422720-q5"
  name         = "wwwserv"
  machine_type = "e2-medium"
  zone         = "us-west1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}